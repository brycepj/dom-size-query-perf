console.log("Page loaded");

function logTiming(key, fn) {
  console.time(key);
  fn();
  console.timeEnd(key);
}

document.addEventListener('DOMContentLoaded', () => {
  setTimeout(() => {
    console.log("**Total Elements** - ", document.getElementsByTagName('*').length);

    logTiming('getElementsByTagName', () => {
      const elems = document.getElementsByTagName('li');
    });

    logTiming('querySelectorAll', () => {
      const elems = document.querySelectorAll('li');
    });

    logTiming('querySelector', () => {
      const elems = document.querySelector('li');
    });

    logTiming('querySelector with class', () => {
      const elems = document.querySelector('.list-item-99');
    });

    logTiming('querySelectorAll with class', () => {
      const elems = document.querySelectorAll('.list-item-99');
    });
  }, 3000);
});
