## 1K Elements, DOM Depth: 2

**Total Elements** -  1007

getElementsByTagName: 0.045166015625ms

querySelectorAll: 0.147216796875ms

querySelector: 0.076904296875ms

querySelector with class: 0.103759765625ms

querySelectorAll with class: 0.14501953125ms

## 100K Elements, DOM Depth: 2

**Total Elements** -  107006

getElementsByTagName: 0.038818359375ms

querySelectorAll: 7.13818359375ms

querySelector: 0.116943359375ms

querySelector with class: 0.093994140625ms

querySelectorAll with class: 2.177001953125ms

## 1M Elements, DOM Depth: 2

**Total Elements** -  1000007

getElementsByTagName: 0.05810546875ms

querySelectorAll: 94.367919921875ms

querySelector: 0.116943359375ms

querySelector with class: 0.086181640625ms

querySelectorAll with class: 78.025146484375ms

## 1.2M Elements, DOM Depth: ~30

**Total Elements** -  1260006

getElementsByTagName: 0.0400390625ms

querySelectorAll: 62.286865234375ms

querySelector: 0.093017578125ms

querySelector with class: 0.076904296875ms

querySelectorAll with class: 22.0849609375ms

